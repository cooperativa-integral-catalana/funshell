#!/bin/bash

<<LICENSE

Copyright (C) 2017  Gioacchino Mazzurco <gio@eigenlab.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

LICENSE

source "$(dirname "${BASH_SOURCE}")/thanks_gnu_parallel.sh"

#[doc] Substitute template named $1 with value $2 in the file list passed on
#[doc] stdin separated by null bytes
#[doc] template example: $%NAME$%
templateSubstitute()
{
	cat | parallel --null sed -i "s/\$%${1}$%/${2}/g"
}
