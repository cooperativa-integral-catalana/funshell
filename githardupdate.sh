#!/bin/bash

<<LICENSE

Copyright (C) 2017  Gioacchino Mazzurco <gio@eigenlab.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

LICENSE

gitHardUpdate()
{
	git fetch --all
	git reset --hard origin/master

	# if there is some new submodule init it
	git submodule update --init --recursive

	git submodule foreach --recursive git fetch --all
	git submodule foreach --recursive git reset --hard origin/master
}
