#!/bin/bash

<<LICENSE

Copyright (C) 2017  Gioacchino Mazzurco <gio@eigenlab.org>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

LICENSE

#[doc] Print a list of file in $1 directory and subtree separated by
#[doc] nullbyte, excluding some better not touch.
#[doc] Common usage is toghether with md5check to see if something as changed
#[doc] before and after running a command
prunedFind()
{
  find $@ \( -path "./.git" -prune -or ! -path "./lastlog" \) -type f -print0
}
